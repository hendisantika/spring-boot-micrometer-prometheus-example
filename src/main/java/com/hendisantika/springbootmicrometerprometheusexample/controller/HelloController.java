package com.hendisantika.springbootmicrometerprometheusexample.controller;

import io.micrometer.core.annotation.Timed;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-micrometer-prometheus-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 25/08/18
 * Time: 07.09
 * To change this template use File | Settings | File Templates.
 */
@RestController
@RequestMapping("/rest")
public class HelloController {

    @Timed(
            value = "hendisantika.hello.request",
            description = "Time taken to return Hello greeting",
            histogram = true,
            percentiles = {0.95, 0.99},
            extraTags = {"version", "1.0"}
    )
    @GetMapping("/hello")
    public String hello() {
        return "Hello Youtube";
    }

    @Timed(
            value = "hendisantika.hello2.request",
            description = "Time taken to return Hello greeting",
            histogram = true,
            percentiles = {0.95, 0.99},
            extraTags = {"version", "1.0"}
    )
    @GetMapping("/hello2")
    public String hello2() {
        return "Hello2 Youtube";
    }

    @Timed(
            value = "hendisantika.hello2.request",
            description = "Time taken to return Hello greeting",
            histogram = true,
            percentiles = {0.95, 0.99},
            extraTags = {"version", "1.0"}
    )
    @GetMapping("/hello3")
    public String hello3() {
        return "Hello3 Youtube";
    }

    @Timed(
            value = "hendisantika.hello3.request",
            histogram = true,
            percentiles = {0.95, 0.99},
            extraTags = {"version", "1.0"}
    )
    @GetMapping("/hello4")
    public String hello4() {
        return "Hello4 Youtube";
    }

}
