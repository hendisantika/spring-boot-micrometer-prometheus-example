# Spring Boot Micrometer with Prometheus Example

* http://localhost:8080/rest/hello,
* http://localhost:8080/rest/hello2,
* http://localhost:8080/rest/hello3,
* http://localhost:8080/rest/hello4 - Different REST endpoints which are collected by Micrometer for Prometheus
* http://localhost:8080/prometheus - Endpoint where the Prometheus metrics can be accessed.

Run prometheus on docker:

```shell
docker run \
    -p 9090:9090 \
    -v /Users/hendisantika/IdeaProjects/spring-boot-micrometer-prometheus-example/prometheus.yml:/etc/prometheus/prometheus.yml \
    prom/prometheus
```

First, let’s check that Prometheus is scraping from our application properly. To do this, we first open up Prometheus by
going to http://localhost:9090 and going to Status > Targets.

The Targets view tells us which applications or endpoints Prometheus is currently scraping from, and also shows the
general health of our scraping:

### Image Screen shot

Prometheus Targets

![Prometheus Targets](img/targets.png "Prometheus Targets")

CPU Usage Metrics

![CPU Usage Metrics](img/cpu.png "CPU Usage Metrics")

HTTP Metrics

![HTTP Metrics](img/http.png "HTTP Metrics")
